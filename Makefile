# Définition des variables
CONTAINER_NAME = demo-php-laravel

install:
	@docker-compose up -d --remove-orphans
	@docker exec -it $(CONTAINER_NAME) composer install
	@docker exec -it $(CONTAINER_NAME) cp .env.example .env
	@docker exec -it $(CONTAINER_NAME) php artisan key:generate
	@docker exec -it $(CONTAINER_NAME) php artisan migrate
	@docker exec -it $(CONTAINER_NAME) php artisan db:seed
	@docker exec -it $(CONTAINER_NAME) php artisan config:cache
	@docker exec -it $(CONTAINER_NAME) php artisan route:cache
	@docker exec -it $(CONTAINER_NAME) php artisan view:cache
	@docker exec -it $(CONTAINER_NAME) php artisan jwt:secret

dev:
# sudo rm -rf ./logs/access.log
	@docker-compose up -d --build --remove-orphans

build:
	@docker-compose build

build-no-cache:
	@docker-compose build --no-cache

workspace:
	@docker exec -it $(CONTAINER_NAME) bash

stop:
	@docker-compose stop

down:
	@docker-compose down

restart:
	@docker-compose restart

migrate:
	@docker exec -it $(CONTAINER_NAME) php artisan migrate:fresh

verif-module-php:
	@docker exec -it $(CONTAINER_NAME) php -m

seed:
	@docker exec -it $(CONTAINER_NAME) php artisan db:seed --class=UserSeeder

clear:
	@docker exec -it $(CONTAINER_NAME) php artisan config:clear
	@docker exec -it $(CONTAINER_NAME) php artisan route:clear
	@docker exec -it $(CONTAINER_NAME) php artisan view:clear

# TEST
migrate-test:
	@docker exec -it $(CONTAINER_NAME) php artisan migrate:fresh --env=testing

seed-test:
	@docker exec -it $(CONTAINER_NAME) php artisan db:seed --class=UserSeeder --env=testing

test:
	php artisan config:clear
	@docker exec -it $(CONTAINER_NAME) /bin/bash -c ./vendor/bin/pest

test-coverage:
	php artisan config:clear
	@docker exec -it $(CONTAINER_NAME) /bin/bash -c "XDEBUG_MODE=coverage ./vendor/bin/pest --coverage-html storage/coverage"