<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Créer des utilisateurs factices
        User::factory()->count(10)->create();

        // Créer un utilisateur qui sera utilisé pour les tests api classic
        User::factory()->create([
            'name' => 'ino',
            'email' => 'ino@test.com',
        ]);

        // Créer un utilisateur qui sera utilisé pour les tests phpunit
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
        ]);
    }
}
