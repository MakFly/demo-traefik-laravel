# Installation du projet

Suivez les étapes ci-dessous pour installer le projet sur votre machine :

1. Clonez le dépôt GitHub :

    ```bash
    git clone https://github.com/votre-utilisateur/votre-projet.git
    ```

2. Accédez au répertoire du projet :

    ```bash
    cd votre-projet
    ```

3. Installez les dépendances :

    ```bash
    npm install
    ```

4. Configurez les variables d'environnement :

    Créez un fichier `.env` à la racine du projet et configurez les variables d'environnement nécessaires.

5. Lancez le serveur de développement :

    ```bash
    npm run dev
    ```

6. Accédez à l'application dans votre navigateur :

    Ouvrez votre navigateur et accédez à l'URL suivante : `http://localhost:3000`

C'est tout ! Vous avez maintenant installé le projet avec succès sur votre machine.


# Tests

Pour lancer les tests, exécutez la commande suivante :

```bash

```