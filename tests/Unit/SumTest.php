<?php

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class);

function sum(int $a, int $b): int
{
    return $a + $b;
}

it('performs sums', function () {
    $result = sum(1, 2);

    expect($result)->toBe(3);
});

it('login', function () {
    User::factory()->create([
        'name' => 'John Doe',
        'email' => 'johndoe@example.com',
    ]);

    $response = $this->postJson('/api/login', [
        'email' => 'johndoe@example.com',
        'password' => 'password',
    ]);

    $response->assertStatus(200);
});
