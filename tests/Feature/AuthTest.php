<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_login()
    {
        $user = User::factory()->create([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com'
        ]);

        $response = $this->post('/api/login', [
            'email' => 'johndoe@example.com',
            'password' => 'password',
        ]);

        $response->assertStatus(200);
        $this->assertAuthenticatedAs($user);
    }

    public function test_user_not_found()
    {
        $response = $this->post('/api/login', [
            'email' => 'kev.aubree@gmail.com',
            'password' => 'password',
        ]);

        $response->assertStatus(401);
        $this->assertGuest();
    }

    public function test_user_cannot_login_with_wrong_password()
    {
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com'
        ]);

        $response = $this->post('/api/login', [
            'email' => 'johndoe@example.com',
            'password' => 'wrong-password',
        ]);

        $response->assertStatus(401);
        
        $this->assertGuest();
    }
}
