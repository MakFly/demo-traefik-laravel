<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateUser()
    {
        $user = User::factory()->create([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
        ]);

        $this->assertDatabaseHas('users', [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
        ]);
    }

    public function testGetUsers()
    {
        // crée moi 10 utilisateurs
        User::factory()->count(10)->create();

        // récupère moi tous les utilisateurs
        $users = User::all();

        // vérifie que j'ai bien 10 utilisateurs
        $this->assertCount(10, $users);
    }
}
