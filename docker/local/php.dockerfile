# STEP 1 : Create a dependency image & create a new image from it
ARG TAG='8.2-fpm'
FROM php:${TAG} AS builder

# Install dependencies
RUN apt-get update && apt-get install -y \
    gnupg \
    g++ \
    procps \
    openssl \
    git \
    unzip \
    zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1

# Create repository directory
WORKDIR /var/www/html

COPY composer.json composer.lock ./
RUN composer install --optimize-autoloader --no-dev --no-scripts --no-progress --no-suggest

# STEP 2 : Create the final image
FROM php:${TAG}

RUN apt-get update && apt-get install -y \
    libpq-dev \
    libzip-dev \
    libfreetype6-dev \
    libpng-dev \
    libjpeg-dev \
    libicu-dev  \
    libonig-dev \
    libxslt1-dev \
    acl \
    poppler-utils \
    && rm -rf /var/lib/apt/lists/*

# Install Postgre PDO
RUN set -ex \
 postgresql-dev

# Install PHP extensions
RUN docker-php-ext-install pdo pdo_pgsql pgsql zip intl gd xsl mbstring opcache pcntl

# Ensure PHP logs are captured by the container
ENV LOG_CHANNEL=stderr

# Install redis
RUN pecl install redis && docker-php-ext-enable redis

# install and enable xdebug
RUN pecl install xdebug-3.2.1 \
    && docker-php-ext-enable xdebug \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Copier le répertoire /var/www/html depuis l'étape de construction
COPY --from=builder /var/www/html /var/www/html

COPY docker/local/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# Configure PHP-FPM
ADD docker/local/php/*.ini /usr/local/etc/php/conf.d/

# Create a user www-data & give it the right to write in the directory
RUN usermod -u 1000 www-data
RUN chown -R www-data:www-data /var/www/html

USER www-data

# Expose port 9000 and start php-fpm server
EXPOSE 9000

# Commande par défaut pour exécuter PHP-FPM
CMD ["php-fpm"]
